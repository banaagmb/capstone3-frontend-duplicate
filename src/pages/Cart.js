import { useEffect, useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";

function Cart() {
  const navigate = useNavigate();
  const [cart, setCart] = useState([]);
  const [cartLength, setCartLength] = useState(0);
  const user = localStorage.getItem("userId");

  const totalPrice = cart.reduce((total, item) => {
    return total + item.price;
  }, 0);

  const token = localStorage.getItem("token");

  function removeToCart(id) {
    fetch(`${process.env.REACT_APP_API_URL}/users/removeCart`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productId: id,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result.cart);
        setCart(result.cart);
      });
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result.cart);
        setCart(result.cart);
        setCartLength(result.cart.length);
      });
  }, [token]);

  console.log(cartLength);

  return user === null ? (
    <Navigate to="/login" />
  ) : (
    <Container className="p-5 mt-5">
      <Row>
        <h1
          className="text-center"
          style={{
            borderBottom: "3px solid black",
            padding: "10px",
            fontWeight: "bold",
          }}
        >
          SHOPPING BAG
        </h1>
      </Row>
      {cart.map((item) => {
        return (
          <Row key={item._id} className="d-flex align-items-center">
            <Col>
              <img src={item.imgSrc} alt="ring" className="img-fluid" />
            </Col>
            <Col>
              <p>{item.productName}</p>
            </Col>
            <Col>
              <p>Quantity: {item.quantity}</p>
            </Col>
            <Col>
              <p>₱ {item.price}</p>
            </Col>
            <Col>
              <Button
                style={{ backgroundColor: "transparent", color: "black" }}
                onClick={() => removeToCart(item.productId)}
              >
                Remove
              </Button>
            </Col>
          </Row>
        );
      })}

      {cartLength === 0 ? (
        <div
          style={{
            height: "50vh",
            marginTop: "50px",
          }}
        >
          <h1>Bag Empty!</h1>
          <p>
            Go to <Link to="/products">Products Page</Link> to add items to cart
          </p>
        </div>
      ) : (
        <Container className="mt-5">
          <Container style={{ backgroundColor: "#d4ddff", padding: "30px" }}>
            <Row>
              <Col>
                <p>Subtotal </p>
              </Col>
              <Col>
                <p style={{ fontWeight: "bold", textAlign: "right" }}>
                  ₱ {totalPrice}
                </p>
              </Col>
            </Row>
            <Row>
              <Col>
                <p>Taxes </p>
              </Col>
              <Col>
                <p style={{ textAlign: "right" }}>--</p>
              </Col>
            </Row>
            <Row style={{ fontWeight: "bold", fontSize: "20px" }}>
              <Col>
                <p>TOTAL</p>
              </Col>
              <Col>
                <p style={{ textAlign: "right" }}>₱ {totalPrice}</p>
              </Col>
            </Row>
          </Container>
          <Row className="justify-content-center">
            <Button
              style={{
                backgroundColor: "black",
                width: "50%",
                marginTop: "60px",
                height: "3.125rem",
                fontWeight: "bolder",
              }}
              onClick={() => navigate("/checkout")}
            >
              CheckOut
            </Button>
          </Row>
          <Row className="justify-content-center">
            <Button
              style={{
                backgroundColor: "transparent",
                width: "50%",
                color: "black",
                marginTop: "20px",
                height: "3.125rem",
                fontWeight: "bolder",
              }}
              onClick={() => navigate("/products")}
            >
              CONTINUE SHOPPING
            </Button>
          </Row>
        </Container>
      )}
    </Container>
  );
}

export default Cart;
