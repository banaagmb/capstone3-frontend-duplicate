import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Button from "@mui/material/Button";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";
import tambunting from "../images/tambunting.jpg";
import manginasal from "../images/manginasal.jpg";
import ml from "../images/ml.jpg";
import palawan from "../images/palawan.jpg";
import Gallery from "react-photo-gallery";
import { photos } from "../images/photos";
import { useNavigate } from "react-router-dom";

import { EffectCoverflow, Pagination, Navigation } from "swiper";

function Home() {
  const logoStyles = { width: "200px", height: "100px" };
  const navigate = useNavigate();
  return (
    <Container fluid>
      <Row className="align-items-center">
        <Col className="p-0" xs={12} lg={8}>
          <img
            src="https://img.freepik.com/free-photo/man-is-holding-woman-s-hand-with-tender-pink-manicure-outdoors_8353-10495.jpg?w=1380&t=st=1680028709~exp=1680029309~hmac=6e476ea0fda8b1f11f1d1bab57ee54d29c6cf74aa83d2cbc21dfc1ef47b37a7c"
            width="100%"
            alt="bride and groom"
          />
        </Col>
        <Col style={{ margin: "50px" }}>
          <Row className="p-3" data-aos="fade-in">
            <img
              src="https://ion.r2net.com/images/Campaigns/DiamondBasics23x1/HPBannerText.png"
              alt="img"
            />
          </Row>
          <Row className="p-4">
            <Button
              variant="outlined"
              size="large"
              onClick={() => navigate("/products")}
              style={{
                color: "black",
                borderColor: "black",
              }}
            >
              SHOP NOW
            </Button>
          </Row>
        </Col>
      </Row>
      <Row className="text-center mt-5 mb-5">
        <Col xs={6} lg={3} data-aos="fade-down">
          <img
            src={tambunting}
            style={logoStyles}
            className="img-fluid mt-3"
            alt="img"
          />
        </Col>
        <Col xs={6} lg={3} data-aos="fade-down">
          <img
            src={ml}
            style={logoStyles}
            className="img-fluid mt-3 animate__fadeInDown animate__animated"
            alt="img"
          />
        </Col>
        <Col xs={6} lg={3} data-aos="fade-down">
          <img
            src={manginasal}
            style={logoStyles}
            className="img-fluid mt-3 animate__fadeInDown animate__animated"
            alt="img"
          />
        </Col>
        <Col xs={6} lg={3} data-aos="fade-down">
          <img
            src={palawan}
            style={logoStyles}
            className="img-fluid mt-3 animate__fadeInDown animate__animated"
            alt="img"
          />
        </Col>
      </Row>
      <Row className="text-center mt-5 mb-3">
        <h1
          style={{ fontSize: "25px" }}
          className="animate__animated animate__fadeIn mb-0 p-0"
        >
          Melúmerri's Best Selling
        </h1>
        <p
          style={{ fontSize: "40px", fontWeight: "inherit" }}
          className="animate__animated animate__fadeIn mt-0 p-0"
        >
          ENGAGEMENT RINGS
        </p>
      </Row>
      <Row>
        <Swiper
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          slidesPerView={"auto"}
          coverflowEffect={{
            rotate: 0,
            stretch: 5,
            depth: 100,
            modifier: 4,
          }}
          pagination={{ el: ".swiper-pagination", clickable: true }}
          navigation={{
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            clickable: true,
          }}
          modules={[EffectCoverflow, Pagination, Navigation]}
          className="mySwiper"
          initialSlide={3}
        >
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/Channel-Set-Engagement-Ring.png"
              alt="img"
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/Halo-Women-Engagement-Ring.png"
              alt="img"
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/eternity-ring.png"
              alt="img"
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/Solitaire-Engagement-Ring.png"
              alt="img"
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/Halo-Women-Engagement-Ring.png"
              alt="img"
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/Three-stone-Engagement-Ring.png"
              alt="img"
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://ion.r2net.com/images/amazingHomepage/rings/Channel-Set-Engagement-Ring.png"
              alt="img"
            />
          </SwiperSlide>
        </Swiper>

        <div className="slider-controler">
          <div className="swiper-button-prev slider-arrow">
            <ion-icon name="arrow-back-outline"></ion-icon>
          </div>
          <div className="swiper-button-next slider-arrow">
            <ion-icon name="arrow-forward-outline"></ion-icon>
          </div>
          <div className="swiper-pagination"></div>
        </div>
      </Row>
      <Row className="text-center mt-5">
        <h1>PICS OR IT DIDN’T HAPPEN!</h1>
        <p>View our customers’ engagement moments from around the world</p>
      </Row>
      <Row className="p-5 ml-5 mr-5">
        <Gallery photos={photos} />
      </Row>
    </Container>
  );
}

export default Home;
