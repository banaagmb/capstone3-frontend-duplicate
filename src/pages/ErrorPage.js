import React from "react";
import { Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

function ErrorPage() {
  return (
    <Container style={{ height: "50vh" }}>
      <Row className="text-center mt-5 mb-5">
        <h1>Error Page</h1>

        <p>We're sorry, but the page you're looking for could not be found.</p>
        <p>
          Go back to <Link to="/">Home</Link>
        </p>
      </Row>
    </Container>
  );
}

export default ErrorPage;
