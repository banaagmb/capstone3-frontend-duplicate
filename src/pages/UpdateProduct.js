import { useRef } from "react";
import { Form, Button, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

function UpdateProduct() {
  const id = useParams();

  console.log(id.prodId);
  const navigate = useNavigate();
  const productName = useRef(null);
  const productPrice = useRef(null);
  const productImage = useRef(null);
  const productDescription = useRef(null);
  const productStocks = useRef(null);

  const token = localStorage.getItem("token");

  function updateProduct() {
    console.log(token);
    fetch(`${process.env.REACT_APP_API_URL}/products/${id.prodId}/update`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },

      body: JSON.stringify({
        name: productName.current.value,
        description: productDescription.current.value,
        price: productPrice.current.value,
        stocks: productStocks.current.value,
        imgSrc: productDescription.current.value,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((result) => {
        console.log(result);
        if (result === true) {
          Swal.fire({
            title: "Product update successful!",
            icon: "success",
          });
        } else {
          Swal.fire({
            title:
              "This functionality can only be accessed by an ADMIN account",
            icon: "error",
          });
        }
      })
      .catch((error) => console.error(error));

    navigate("/admin");
  }

  return (
    <Container fluid className="p-4">
      <h2>UPDATE PRODUCT</h2>
      <Form onSubmit={updateProduct}>
        <Form.Group className="mb-3">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter name"
            ref={productName}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Price"
            ref={productPrice}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Image</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter image url"
            ref={productImage}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter description"
            ref={productDescription}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Stocks</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter number of stocks"
            ref={productStocks}
          />
        </Form.Group>

        <Button variant="primary" type="submit" className="mt-3">
          Save
        </Button>
      </Form>
    </Container>
  );
}

export default UpdateProduct;
