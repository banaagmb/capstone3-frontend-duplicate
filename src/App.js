import ResponsiveAppBar from "./components/ResponsiveAppBar";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import Home from "./pages/Home";
import ErrorPage from "./pages/ErrorPage";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Logout from "./pages/Logout";
import Cart from "./pages/Cart";
import Profile from "./pages/Profile";
import Products from "./pages/Products";
import FAQS from "./pages/FAQS";
import SpecificProduct from "./pages/SpecificProduct";
import Footer from "./components/Footer";
import Admin from "./pages/Admin";
import CreateProduct from "./pages/CreateProduct";
import ThankYou from "./pages/ThankYou";
import SearchList from "./pages/SearchList";
import CheckOut from "./pages/CheckOut";

function App() {
  return (
    <Router>
      <ResponsiveAppBar />
      <Container fluid className="p-0 m-0">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/admin" element={<Admin />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/products" element={<Products />} />
          <Route path="/faqs" element={<FAQS />} />
          <Route path="/faqs" element={<SearchList />} />
          <Route path="/ring/:prodId" element={<SpecificProduct />} />
          <Route path="/search/:item" element={<SearchList />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/checkout" element={<CheckOut />} />
          <Route path="/thankyou" element={<ThankYou />} />
          <Route path="/create" element={<CreateProduct />} />

          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Container>
      <Footer />
    </Router>
  );
}

export default App;
