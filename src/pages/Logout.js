import React from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userActions } from "../reducer/user-slice";

function Logout() {
  const userDetails = useSelector((state) => state.userReducer.userDetails);
  const dispatch = useDispatch();

  localStorage.clear();

  // dispatch(userActions.logoutUser());

  return <Navigate to="/login" />;
}

export default Logout;
