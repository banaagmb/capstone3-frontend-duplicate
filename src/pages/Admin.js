import { useEffect, useState, useRef } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Table,
  Modal,
  Form,
  Tab,
  Tabs,
} from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import AddBoxIcon from "@mui/icons-material/AddBox";

function Admin() {
  const [productsAdmin, setProductsAdmin] = useState([]);
  const [activeUsers, setActiveUsers] = useState([]);
  const [userIsAdmin, setUserIsAdmin] = useState(true);
  const navigate = useNavigate();

  const [itemId, setItemId] = useState("");
  const [itemName, setItemName] = useState("");
  const [itemIsActive, setItemIsActive] = useState(true);
  const [isSetAdmin, setIsSetAdmin] = useState(false);

  const [showArchiveModal, setShowArchiveModal] = useState(false);
  const handleArchiveClose = () => setShowArchiveModal(false);

  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const handleUpdateClose = () => setShowUpdateModal(false);

  const [showStocksModal, setShowStocksModal] = useState(false);
  const handleStocksClose = () => setShowStocksModal(false);

  const [showSetAdminModal, setShowSetAdminModal] = useState(false);
  const handleSetAdminClose = () => setShowSetAdminModal(false);

  const productName = useRef(null);
  const productPrice = useRef(null);
  const productImage = useRef(null);
  const productDescription = useRef(null);

  // const stocksToAdd = useRef("");
  const [stocksToAdd, setStocksToAdd] = useState(0);

  const [key, setKey] = useState("products");

  const token = localStorage.getItem("token");

  useEffect(() => {
    //FETCH PRODUCTS
    fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProductsAdmin(data);
      })
      .catch((error) => console.error(error));

    //FETCH USERS

    fetch(`${process.env.REACT_APP_API_URL}/users/`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setActiveUsers(data);
      })
      .catch((error) => console.error(error));

    //VERIFY IF ADMIN

    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.isAdmin !== true) {
          setUserIsAdmin(false);
          navigate("*");
        }
      });
  }, [navigate, token]);

  function addStock(id) {
    console.log(stocksToAdd);
    console.log(typeof stocksToAdd);

    fetch(`${process.env.REACT_APP_API_URL}/products/${itemId}/addStocks`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },

      body: JSON.stringify({
        stocks: stocksToAdd,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((result) => {
        console.log(result);
        if (result === true) {
          Swal.fire({
            title: "Stocks successfully added!",
            text: "Please reload page to view changes.",
            icon: "success",
          });
        } else {
          Swal.fire({
            title:
              "This functionality can only be accessed by an ADMIN account",
            icon: "error",
          });
        }
      })
      .catch((error) => console.error(error));
  }

  function archiveProduct(id) {
    console.log(id);
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        if (res) {
          Swal.fire({
            title: "Archiving product success!",
            icon: "success",
          });

          window.location.reload(false);
        } else {
          Swal.fire({
            title: "Archiving product failed",
            icon: "error",
            text: "Something went wrong. Please try again",
          });
        }
      });
  }

  function unArchiveProduct(id) {
    console.log(id);
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        if (res) {
          Swal.fire({
            title: "Unarchived product success!",
            icon: "success",
          });

          window.location.reload(false);
        } else {
          Swal.fire({
            title: "Unarchiving product failed",
            icon: "error",
            text: "Something went wrong. Please try again",
          });
        }
      });
  }

  function updateProduct(id) {
    console.log(id);

    fetch(`${process.env.REACT_APP_API_URL}/products/${itemId}/update`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },

      body: JSON.stringify({
        name: productName.current.value,
        description: productDescription.current.value,
        price: parseInt(productPrice.current.value),
        imgSrc: productDescription.current.value,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((result) => {
        console.log(result);
        if (result === true) {
          Swal.fire({
            title: "Product update successful!",
            text: "Please reload page to view changes.",
            icon: "success",
          });
        } else {
          Swal.fire({
            title:
              "This functionality can only be accessed by an ADMIN account",
            icon: "error",
          });
        }
      })
      .catch((error) => console.error(error));
  }

  function makeAdmin(id) {
    console.log(id);

    fetch(`${process.env.REACT_APP_API_URL}/users/makeAdmin`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        isAdmin: true,
        userId: id,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        if (res) {
          Swal.fire({
            title: "User made ADMIN success!",
            icon: "success",
          });

          window.location.reload(false);
        } else {
          Swal.fire({
            title: "User made ADMIN failed",
            icon: "error",
            text: "Something went wrong. Please try again",
          });
        }
      });
  }

  function makeNonAdmin(id) {
    console.log(id);

    fetch(`${process.env.REACT_APP_API_URL}/users/makeAdmin`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        isAdmin: false,
        userId: id,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        if (res) {
          Swal.fire({
            title: "User made NON-ADMIN success!",
            icon: "success",
          });

          window.location.reload(false);
        } else {
          Swal.fire({
            title: "User made NON-ADMIN failed",
            icon: "error",
            text: "Something went wrong. Please try again",
          });
        }
      });
  }

  return (
    <Container fluid className="p-3">
      <h1>ADMIN DASHBOARD</h1>
      <Tabs
        id="controlled-tab-example"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        className="mb-3"
      >
        <Tab eventKey="users" title="Users">
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>EMAIL</th>
                <th>FIRST NAME</th>
                <th>LAST NAME</th>
                <th>MOBILE NUMBER</th>
                <th>IS ADMIN</th>
                <th>MODIFY</th>
              </tr>
            </thead>
            <tbody>
              {activeUsers.map((user) => (
                <tr key={user._id}>
                  <td>{user.email}</td>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                  <td>{user.mobileNumber}</td>
                  <td>{JSON.stringify(user.isAdmin)}</td>
                  <td>
                    {user.isAdmin ? (
                      <Button
                        style={{ marginRight: "5px" }}
                        onClick={() => {
                          setShowSetAdminModal(true);
                          setItemId(user._id);
                          setIsSetAdmin(user.isAdmin);
                        }}
                      >
                        SET AS NON ADMIN
                      </Button>
                    ) : (
                      <Button
                        style={{ marginRight: "5px", marginTop: "5px" }}
                        onClick={() => {
                          setShowSetAdminModal(true);
                          setItemId(user._id);
                          setIsSetAdmin(user.isAdmin);
                        }}
                      >
                        SET AS ADMIN
                      </Button>
                    )}

                    <Modal
                      show={showSetAdminModal}
                      onHide={handleSetAdminClose}
                      centered
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>SET USER AS ADMIN/NON-ADMIN</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        Are you sure you want to make user admin/non-admin?
                      </Modal.Body>
                      <Modal.Footer>
                        <Button
                          variant="secondary"
                          onClick={handleSetAdminClose}
                        >
                          Close
                        </Button>
                        {isSetAdmin ? (
                          <Button
                            variant="primary"
                            onClick={() => makeNonAdmin(itemId)}
                          >
                            MAKE NON-ADMIN
                          </Button>
                        ) : (
                          <Button
                            variant="primary"
                            onClick={() => makeAdmin(itemId)}
                          >
                            MAKE ADMIN
                          </Button>
                        )}
                      </Modal.Footer>
                    </Modal>
                    <Button style={{ marginTop: "5px" }}>Delete user</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Tab>

        <Tab eventKey="orders" title="Orders">
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>PRODUCT NAME</th>
                <th>TRANSACTION DATE</th>
                <th>QUANTITY</th>
                <th>CUSTOMER EMAIL</th>
              </tr>
            </thead>
            <tbody>
              {productsAdmin.map((item) => {
                return (
                  <>
                    {item.orders.map((order) => {
                      return (
                        <tr key={order._id}>
                          <td>{item.name}</td>
                          <td>{order.purchasedOn}</td>
                          <td>{order.quantity}</td>
                          <td>{order.userEmail}</td>
                        </tr>
                      );
                    })}
                  </>
                );
              })}
            </tbody>
          </Table>
        </Tab>

        <Tab eventKey="products" title="Products">
          <Table striped bordered hover responsive>
            <thead>
              <tr>
                <th>PRODUCT ID</th>
                <th>PRODUCT NAME</th>
                <th>PRICE</th>
                <th>IS ACTIVE</th>
                <th>IS SALE</th>
                <th>STOCKS</th>
                <th>MODIFY</th>
              </tr>
            </thead>
            <tbody>
              {productsAdmin.map((item) => (
                <tr key={item._id}>
                  <td>{item._id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>{JSON.stringify(item.isActive)}</td>
                  <td>{JSON.stringify(item.isSale)}</td>
                  <td>
                    {item.stocks}
                    <AddBoxIcon
                      onClick={() => {
                        setShowStocksModal(true);
                        setItemId(item._id);
                      }}
                    />
                  </td>

                  <Modal show={showStocksModal} onHide={handleStocksClose}>
                    <Modal.Header closeButton>
                      <Modal.Title>ADD STOCKS</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Form.Group className="mb-3">
                          <Form.Label>Quantity</Form.Label>
                          <Form.Control
                            type="number"
                            placeholder="Enter number of stocks"
                            value={stocksToAdd}
                            onChange={(e) =>
                              setStocksToAdd(parseInt(e.target.value))
                            }
                          />
                        </Form.Group>
                      </Form>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleStocksClose}>
                        Close
                      </Button>
                      <Button
                        variant="primary"
                        onClick={() => addStock(itemId)}
                      >
                        Save Changes
                      </Button>
                    </Modal.Footer>
                  </Modal>
                  <td>
                    <Button
                      style={{ marginRight: "5px", marginTop: "5px" }}
                      onClick={() => {
                        setShowUpdateModal(true);
                        setItemId(item._id);
                        setItemName(item.name);
                      }}
                    >
                      Update
                    </Button>

                    {item.isActive ? (
                      <Button
                        style={{ marginTop: "5px" }}
                        onClick={() => {
                          setShowArchiveModal(true);
                          setItemId(item._id);
                          setItemName(item.name);
                          setItemIsActive(item.isActive);
                        }}
                      >
                        Archive
                      </Button>
                    ) : (
                      <Button
                        style={{ marginTop: "5px" }}
                        onClick={() => {
                          setShowArchiveModal(true);
                          setItemId(item._id);
                          setItemName(item.name);
                          setItemIsActive(item.isActive);
                        }}
                      >
                        Unarchive
                      </Button>
                    )}

                    <Modal
                      show={showArchiveModal}
                      onHide={handleArchiveClose}
                      centered
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Archive Product</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        Are you sure you want to archive {itemName}?
                      </Modal.Body>
                      <Modal.Footer>
                        <Button
                          variant="secondary"
                          onClick={handleArchiveClose}
                        >
                          Close
                        </Button>
                        {itemIsActive ? (
                          <Button
                            variant="primary"
                            onClick={() => archiveProduct(itemId)}
                          >
                            Archive
                          </Button>
                        ) : (
                          <Button
                            variant="primary"
                            onClick={() => unArchiveProduct(itemId)}
                          >
                            Unarchive
                          </Button>
                        )}
                      </Modal.Footer>
                    </Modal>

                    <Modal show={showUpdateModal} onHide={handleUpdateClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>UPDATE PRODUCT</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <Form>
                          <Form.Group className="mb-3">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Enter name"
                              ref={productName}
                            />
                          </Form.Group>
                          <Form.Group className="mb-3">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                              type="number"
                              placeholder="Enter Price"
                              ref={productPrice}
                            />
                          </Form.Group>
                          <Form.Group className="mb-3">
                            <Form.Label>Image</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Enter image url"
                              ref={productImage}
                            />
                          </Form.Group>
                          <Form.Group className="mb-3">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Enter description"
                              ref={productDescription}
                            />
                          </Form.Group>
                        </Form>
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleUpdateClose}>
                          Close
                        </Button>
                        <Button
                          variant="primary"
                          onClick={() => updateProduct(itemId)}
                        >
                          Save Changes
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>

          <Row>
            <Col className="mt-3 mb-5">
              <Button
                onClick={() => {
                  navigate("/create");
                }}
              >
                Add New Product
              </Button>
            </Col>
          </Row>
        </Tab>
      </Tabs>
    </Container>
  );
}

export default Admin;
