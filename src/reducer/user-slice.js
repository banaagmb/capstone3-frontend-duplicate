import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "useDetails",
  initialState: { userDetails: {} },

  reducers: {
    loginUser(state, action) {
      const userLoginDetails = action.payload;

      console.log(`useDetails payload: ${userLoginDetails}`);
      state.userDetails = userLoginDetails;


    },

    logoutUser(state) {
      state.userDetails = null;


    },
  },
});

export const userActions = userSlice.actions;

export default userSlice;
