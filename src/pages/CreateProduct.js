import { useRef } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

function CreateProduct() {
  const navigate = useNavigate();
  const productName = useRef(null);
  const productPrice = useRef(null);
  const productImage = useRef(null);
  const productDescription = useRef(null);
  const productStocks = useRef(null);

  const token = localStorage.getItem("token");

  function addProduct() {
    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },

      body: JSON.stringify({
        name: productName.current.value,
        description: productDescription.current.value,
        price: productPrice.current.value,
        stocks: productStocks.current.value,
        imgSrc: productDescription.current.value,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((result) => {
        console.log(result);
        if (result == true) {
          Swal.fire({
            title: "New Product Added!",
            icon: "success",
          });
        } else {
          Swal.fire({
            title:
              "This functionality can only be accessed by an ADMIN account",
            icon: "error",
          });
        }
      })
      .catch((error) => console.error(error));

    navigate("/admin");
  }

  return (
    <Form onSubmit={addProduct} style={{ padding: "50px" }}>
      <h1 className="mb-5">CREATE PRODUCT</h1>
      <Form.Group className="mb-3">
        <Form.Label>Product Name</Form.Label>
        <Form.Control type="text" placeholder="Enter name" ref={productName} />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter Price"
          ref={productPrice}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Image</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter image url"
          ref={productImage}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter description"
          ref={productDescription}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Stocks</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter number of stocks"
          ref={productStocks}
        />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}

export default CreateProduct;
