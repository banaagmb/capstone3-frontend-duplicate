export const photos = [
  {
    src: "https://img.freepik.com/free-photo/beautiful-wedding-ceremony-nature_52683-90895.jpg?w=1380&t=st=1679306811~exp=1679307411~hmac=18627f8053eac4179333714cc70fcbbb6dc25e6206998392c52c0d7e893fc122",
    width: 10,
    height: 5,
  },
  {
    src: "https://img.freepik.com/free-photo/relationship-lovely-couple-cafe_144627-40291.jpg?size=626&ext=jpg&ga=GA1.1.522437817.1679213670&semt=ais",
    width: 3,
    height: 2,
  },
  {
    src: "https://img.freepik.com/free-photo/handsome-man-placing-wedding-ring-blonde-woman-s-hand-restaurant_23-2147891148.jpg?w=1380&t=st=1679306920~exp=1679307520~hmac=d34996940b7d2ae72e708ced0c57746fb5b65e3ef457fb716b3f6f5305ce97c3",
    width: 4,
    height: 3,
  },
  {
    src: "https://img.freepik.com/free-photo/ring-box_1098-14934.jpg?w=1380&t=st=1679306913~exp=1679307513~hmac=75e9b51e5acf6506b92b20519beb5d5c36900d79e81f57d243ae17980937dc45",
    width: 5,
    height: 4,
  },
  {
    src: "https://img.freepik.com/free-photo/proposal-romantic-place_329181-6082.jpg?w=1380&t=st=1679306973~exp=1679307573~hmac=87444dd16c2cfe4c9409a8f91cfca50134777f5555b8c7f8db40b0906017af9a",
    width: 8,
    height: 5,
  },
  {
    src: "https://img.freepik.com/free-photo/man-holding-wedding-ring-front-astonished-happy-girl-covering-mouth-with-hand-romantic-photo-charming-woman-standing-roof-early-evening-date-with-boyfriend-anniversary_197531-25338.jpg?w=1380&t=st=1679306966~exp=1679307566~hmac=b9bb47545e782c293689d3c6c6643ed537360c9408f74885e08e43ab1eed041e",
    width: 4,
    height: 3,
  },
  {
    src: "https://img.freepik.com/free-photo/happy-couple-with-gifts_624325-2121.jpg?w=1380&t=st=1679308797~exp=1679309397~hmac=7474dc0c4c8c2a717cc8d0723bcaeca9758b78f1c33129289388355756e1fc6a",
    width: 4,
    height: 3,
  },
  {
    src: "https://img.freepik.com/free-photo/happy-portrait-couple-surprise-marriage_1150-11966.jpg?w=1380&t=st=1679308802~exp=1679309402~hmac=0483104518df255423cb16c02a6134584567a7d5c2ce65f2fbf552839078a1c8",
    width: 4,
    height: 3,
  },
  {
    src: "https://img.freepik.com/free-photo/couple-celebrating-engagement-together_23-2149430737.jpg?w=1380&t=st=1679308805~exp=1679309405~hmac=34e60642fb19eab12b4f465bebaf45fb873f78cea1f3c11452a8a96748c18316",
    width: 4,
    height: 3,
  },
  {
    src: "https://img.freepik.com/free-photo/proposal-from-african-boy-caucasian-girl-terrace-cosy-open-air-restaurant_8353-9757.jpg?w=1380&t=st=1679308808~exp=1679309408~hmac=10b60d4cd41a44d6f38c52f2835c61db8d60614c5c6dfeef0efae3f2f4eb2e77",
    width: 4,
    height: 3,
  },
  {
    src: "https://img.freepik.com/free-photo/fiancee_1098-15223.jpg?w=1380&t=st=1679308919~exp=1679309519~hmac=d2dbcab0e7379d7a40f1828b8931ed89271c5c7b5bf5c8dfc971827a57fff884",
    width: 4,
    height: 3,
  },
];
