import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Container, Row, Col, Card } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";

function SearchList() {
  const [productItems, setProductsItems] = useState([]);

  const { item } = useParams();

  console.log(item);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/search/${item}`)
      .then((res) => res.json())
      .then((products) => {
        if (products.length !== 0) {
          setProductsItems(
            <Container className="mt-5">
              <Row style={{ borderBottom: "5px solid black" }}>
                <h5>
                  Results for <i>" {item} "</i>
                </h5>
              </Row>
              <Row className="mt-5">
                {products.map((item) => {
                  return (
                    <Col key={item._id} lg={6} data-aos="fade-in">
                      <Link to={`/ring/${item._id}`}>
                        <Card>
                          <Card.Img variant="top" src={item.imgSrc} />
                          <Card.Body>
                            <Card.Title>{item.name}</Card.Title>

                            <Card.Text>{`₱${item.price}`}</Card.Text>
                          </Card.Body>
                        </Card>
                      </Link>
                    </Col>
                  );
                })}
              </Row>
            </Container>
          );
        } else {
          setProductsItems(
            <Container className="mt-5">
              <Row style={{ borderBottom: "5px solid black" }}>
                <h5>
                  Results for <i>" {item} "</i>
                </h5>
              </Row>
              <Row className="mt-5 mb-5" style={{ height: "40vh" }}>
                <h6>No result found</h6>
              </Row>
            </Container>
          );
        }
      });
  }, [item]);

  return <Container>{productItems}</Container>;
}

export default SearchList;
